using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Target : MonoBehaviour
{
    private float minSpeed = 12;
    private float maxSpeed = 16;
    private float maxTorque = 10;
    private float xRangeSpawn = 4.35f;
    private float yMaxSpawn = -2.5f;


    private Rigidbody rb;

    private bool isFreezed;
    public bool IsFreezed => isFreezed;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    Vector3 RandomForce()
    {
        return Vector3.up * Random.Range(minSpeed, maxSpeed);
    }
    float RandomTorque()
    {
        return Random.Range(-maxTorque, maxTorque);
    }
    Vector3 RandomPos()
    {
        return new Vector3(Random.Range(-xRangeSpawn, xRangeSpawn), yMaxSpawn, 1);
    }
    public void ForceStrength()
    {
        
        rb.AddForce(RandomForce(), ForceMode.Impulse);
        rb.AddTorque(RandomTorque(), RandomTorque(), RandomTorque(), ForceMode.Impulse);
        transform.position = RandomPos();
    }
    public void FreezeObjectsInScene()
    {
        StartCoroutine(FreezeObjects());
       
    }
    public IEnumerator FreezeObjects()
    {
        rb.constraints = RigidbodyConstraints.FreezePosition;
        isFreezed = true;
        yield return new WaitForSeconds(3f);
        isFreezed = false;
        rb.constraints = RigidbodyConstraints.None;
    }
    public abstract void DestroyTarget();

}
