using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class GameManager : MonoBehaviour
{

    [SerializeField] private List<Target> targets;
    [SerializeField] private float spawnRate;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI bestScoreText;
    [SerializeField] private TextMeshProUGUI gameOverText;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private GameObject titleScreen;
    [SerializeField] private GameObject pauseScreen; 
    [SerializeField] private Button restartButton;
    [SerializeField] private Button pauseButton;
    [SerializeField] private Button homeButton;
    [SerializeField] private Button continueButton;

    private int lives;
    private int score;
    private bool paused = false;

    public bool isGameActive;

    public bool IsGameActive => isGameActive;
    public int Lives => lives;
    private void Awake()
    {
        LoadPoints();
    }
    private void Start()
    {
        pauseButton.onClick.AddListener(Paused);
        homeButton.onClick.AddListener(HomeButtonGame);
        continueButton.onClick.AddListener(ContinueGame);
    }
    void Paused()
    {
        if (!paused)
        {
            paused = true; 
            pauseScreen.SetActive(true);
            Time.timeScale = 0;
        }
    }
    void ContinueGame()
    {
        if (paused)
        {
            paused = false;
            pauseScreen.SetActive(false);
            Time.timeScale = 1;
        }
    }
    IEnumerator SpawnTarget()
    {
        while(isGameActive)
        {
            yield return new WaitForSeconds(spawnRate);
            int randIndex = Random.Range(0, 3);
            Instantiate(targets[randIndex], this.transform);
            if (GetComponentInChildren<ObstacleTarget>().IsFreezed)
            {
                yield return new WaitForSeconds(3f);
            }
        }
    }
    IEnumerator SpawnFreezePickup()
    {
        while (isGameActive)
        {
            yield return new WaitForSeconds(6f);
            Instantiate(targets[4], this.transform);
            if (GetComponentInChildren<Target>().IsFreezed)
            {
                yield return new WaitForSeconds(3f);
            }
        }
    }
    IEnumerator SpawnHealthPickup()
    {
        while (isGameActive)
        {
            yield return new WaitForSeconds(8.5f);
            Instantiate(targets[5], this.transform);
            
        }
    }

    public void UpdateScore(int scoreToUpdate)
    {
        score += scoreToUpdate;
        scoreText.text = "Score: " + score;
    }
    public void UpdateLives(int livesToChange)
    { 
      
        lives += livesToChange; 
        livesText.text = "Lives: " + lives; 
        if (lives <= 0) 
        { 
            GameOver(); 
        } 
    }
    public void GameOver()
    {
        SaveData data = new SaveData();
        gameOverText.gameObject.SetActive(true);
        isGameActive = false;
        restartButton.gameObject.SetActive(true);
        if (score > data.score)
        {
           SavePoints();
        }
       
    }
    public void HomeButtonGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void StartGame(int difficulty)
    {
        isGameActive = true;
        score = 0;
        spawnRate = spawnRate / difficulty;


        StartCoroutine(SpawnTarget());
        StartCoroutine(SpawnFreezePickup());
        StartCoroutine(SpawnHealthPickup());
        UpdateScore(0);
        UpdateLives(10);

        pauseButton.gameObject.SetActive(true);
        homeButton.gameObject.SetActive(true);

        titleScreen.gameObject.SetActive(false);
    }
    [System.Serializable]
    public class SaveData
    {
        public int score = 0;
    }
    public void SavePoints()
    {
        SaveData data = new SaveData();

        data.score = score;
        string json = JsonUtility.ToJson(data);

        File.WriteAllText(Application.persistentDataPath + "/savefile.json", json);

    }
    public void LoadPoints()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            SaveData data = JsonUtility.FromJson<SaveData>(json);
            bestScoreText.text = "Best Score: " + data.score;
        }
    }

}
