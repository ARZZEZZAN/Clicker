using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleTarget : Target
{
    [SerializeField] private int pointValue;
    [SerializeField] private ParticleSystem explosionParticle;
    private GameManager gameManager;
    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        ForceStrength();
    }
    public override void DestroyTarget()
    {
        if (gameManager.IsGameActive)
        {
            Destroy(gameObject);
            Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);
            gameManager.UpdateScore(pointValue);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
        FailedObjectsCheck();
    }
    private void FailedObjectsCheck()
    {
        if (!gameObject.CompareTag("Bad") && gameManager.IsGameActive)
        {
            gameManager.UpdateLives(-1);

        }
    }
    
}
