using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TrailRenderer), typeof(BoxCollider))]

public class ClickAndSwipe : MonoBehaviour
{

    private GameManager gameManager;
    private Camera cam;
    private Vector3 mousePos;
    private TrailRenderer trail;
    private BoxCollider col;
    private bool swiping = false;


    void Awake()
    {
        cam = Camera.main;
        trail = GetComponent<TrailRenderer>();
        col = GetComponent<BoxCollider>();
        trail.enabled = false;
        col.enabled = false;

    }
    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            
    }
    void UpdateMousePosition()
    {
        mousePos = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f));
        transform.position = mousePos;
    }
    void UpdateComponents()
    {
        trail.enabled = swiping;
        col.enabled = swiping;
    }
    void Update()
    {


        if (gameManager.IsGameActive)
        {
            if (Input.GetMouseButtonDown(0))
            {
                swiping = true; 
                UpdateComponents();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                swiping = false; 
                UpdateComponents();
            }
            if (swiping)
                UpdateMousePosition();


        }
    }
    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.GetComponent<ObstacleTarget>())
        {
            collision.gameObject.GetComponent<ObstacleTarget>().DestroyTarget();
        }
        if (collision.gameObject.GetComponent<PickupFreezeTarget>())
        {
            
            GameObject[] targets = GameObject.FindGameObjectsWithTag("Obstacle");
            foreach (var target in targets)
            {
                target.GetComponent<Target>().FreezeObjectsInScene();
            }
            collision.gameObject.GetComponent<PickupFreezeTarget>().DestroyTarget();

        }
        if (collision.gameObject.GetComponent<PickupHealthTarget>())
        {
            collision.gameObject.GetComponent<PickupHealthTarget>().DestroyTarget();
        }

    }
}
