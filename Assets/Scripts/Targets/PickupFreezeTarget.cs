using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupFreezeTarget : Target
{
    [SerializeField] private ParticleSystem explosionParticle;

    private GameManager gameManager;
    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        ForceStrength();
    }
    public override void DestroyTarget()
    {
        if (gameManager.IsGameActive)
        {
            Destroy(gameObject);
            Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
