using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupHealthTarget : Target
{
    [SerializeField] private ParticleSystem explosionParticle;

    private GameManager gameManager;
    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        ForceStrength();
    }
    public override void DestroyTarget()
    {
        if (gameManager.IsGameActive)
        {
            Destroy(gameObject);
            Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);

            if(gameManager.Lives < 10 && gameManager.Lives > 8)
            {
                gameManager.UpdateLives(1);
            }
            else if (gameManager.Lives == 10)
            {
                gameManager.UpdateLives(0);
            }
            else
            {
                gameManager.UpdateLives(2);
            }
            
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
